class Particle {
    constructor(x, y, r, colorNumber, player = false) {
        this.position = createVector(x, y, 0);
        this.r = r;
        this.color = colorNumber ? colorNumber : color(random(0, 255), random(0, 255), random(0, 255));
        this.velocity = createVector(0, 0, 0);
        this.isPlayer = player;
        this.isCrashed = false;
    }

    update() {
        if(!this.isCrashed){
            this.position = this.position.add(this.velocity);
        }
    }

    addAcceleration(accelerationVector) {
        p5.Vector.add(this.velocity, accelerationVector, this.velocity);
    }

    updateIsCrashed(other) {
        if(this.isCrashed)
            return this.isCrashed;

        const x = this.position.x;
        const y = this.position.y;
        const z = this.position.z;

        if(abs(x - other.position.x) < this.r) {
            if(abs(y - other.position.y) < this.r) {
                if(abs(z - other.position.z) < this.r) {
                    this.isCrashed = true;
                }
            }
        }
        return this.isCrashed;
    }

    show() {
        if(!this.isCrashed) {
            noStroke()

            push();
            translate(this.position.x, this.position.y, this.position.z, 150, 150);
            if(this.isPlayer) {
                ellipsoid(this.r, this.r / 2, this.r);
            }
            else {
                fill(this.color);
                sphere(this.r, 10, 10);
            }
            pop();
        }
    }
}