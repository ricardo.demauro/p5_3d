const particles = new Array(3);
const particleSize = 25;
let player;
const acceConst = 0.61;

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);

  const w = width / 2;
  const h = height / 2;
  for(let i = 0, len = particles.length; i < len; i++) {
    particles[i] = new Particle(random(-w, w), random(-h, h), 25);
  }
  player = new Particle(0, 0, 45, 125, true);

  console.log(width)
}

function draw() {
  background(51);
  //box();

  //translate(width/2,height/2,0); //moves our drawing origin to the top left corner
  //box();

  particles.forEach(particle => {
    particle.updateIsCrashed(player);
    
    particle.update();
    particle.show();
  });

  player.update();
  player.show();
}

function keyPressed() {
  let acceleration = createVector(0, 0);
  switch (key) {
    case 'w':
      acceleration.add(0, -acceConst);
      break;
    case 's':
      acceleration.add(0, acceConst);
      break;
    case 'a':
      acceleration.add(-acceConst, 0);
      break;
    case 'd':
      acceleration.add(acceConst, 0);
      break;
    default:
      break;
  }
  player.addAcceleration(acceleration);
}